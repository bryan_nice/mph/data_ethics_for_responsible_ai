# Data Ethics for Responsible AI

## Intention for this repo

This repo is a workspace supporting the research, and experimentation I am doing to complete my thesis (practicum) to complete my MPH. I am openly sharing to broader healthcare and public health communities to encourage participation, recommendations, and contributions from others. Currently, I am in the early stages of this work and completed my literature review. This work adheres to [semantic version standards](https://semver.org/).

Roadmap ToDo's:

+ [ ] Playbooks to get started
+ [ ] Platform Reference Architecture
+ [ ] Exploratory Data Analysis Practicesss

## Description of the problem space

There is a lot of hype to use advanced analytics methods within healthcare using Artificial Intelligence (AI)/ Machine Learning (ML) models. Public health and clinical leaders are constantly being bombarded to support buying tools or technology with untested hope it will improve patient outcomes. Data scientists and engineers are becoming more seen fiduciarily responsible partners to clinicians with skills to assess technology in a care setting.[1] However, they do not have the same level of clinical awareness to know if their recommendations may harm patients.[1] The data definition for a given population inherently has bias, even though it may have statistical confidence.[2]

The concept of responsible AI and data ethics has become a major topic of interest, especially in healthcare and policy. EU General Data Protection Regulation (GDPR) has written provisions on explainability and interpretability of AI, National Institute of Standards and Technology (NIST) is currently developing an AI Risk management framework, and Food and Drug Administration (FDA) has a proposed regulatory framework, yet none has identified method, checklist, or 
framework to evaluate bias within the predictive model development lifecycle. This literature review is focused on identifying an approach, checklist, or framework, from a data ethics perspective, to identify intersectional bias and mitigation algorithmic harm in health care.

## License

[GPLv3](LICENSE)

## References

1. Montague E, Day TE, Barry D, Brumm M, McAdie A, Cooper AB, et al. The case for information fiduciaries: The implementation of a data ethics checklist at Seattle Children’s Hospital. J Am Med Inform Assoc [Internet]. 2021 [cited 2022 Oct 15];28(3):650–2. Available from: http://dx.doi.org/10.1093/jamia/ocaa307

2. Kleinberg J, Mullainathan S, Raghavan M. Inherent trade-offs in the fair determination of risk scores. arXiv [csLG] [Internet]. 2016 [cited 2022 Oct 15]; Available from: http://arxiv.org/abs/1609.05807

