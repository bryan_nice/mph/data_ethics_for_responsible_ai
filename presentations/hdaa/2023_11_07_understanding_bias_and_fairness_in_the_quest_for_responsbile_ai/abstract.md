# Abstract

Understanding the difference between data bias and algorithmic harm is crucial for clinical and public health leaders, as it enables them to identify and address the root causes of health disparities. Data bias refers to inaccuracies or misrepresentations in the data, often caused by underrepresentation of certain population groups. Algorithmic harm, on the other hand, results from biased algorithms that perpetuate or exacerbate existing disparities, leading to negative consequences for vulnerable populations.

By recognizing the distinction between data bias and algorithmic harm, organizations can adopt a data ethics approach combined with systems thinking to effectively utilize qualitative studies in improving equitable access to care. This strategy enables healthcare organizations to work towards fairer healthcare systems that consider the needs of all population groups, including those who are underrepresented.

This presentation will provide valuable insights and stimulate essential discussions on the importance of mitigating intersectional bias in healthcare algorithms to ensure equitable outcomes. By incorporating data ethics and systems thinking, clinical and public health leaders can better understand the complexities of health disparities and work towards creating a more just and inclusive healthcare environment for all.

# Learning Objectives

1. Understand the difference between data bias and algorithmic harm, and their impacts on health disparities and vulnerable populations.
2. Recognize the importance of incorporating data ethics and systems thinking in addressing intersectional bias within healthcare algorithms.
3. Learn how to effectively utilize qualitative studies to improve equitable access to care and contribute to a more just and inclusive healthcare environment.
