WITH idx_nt_tg AS (
    SELECT
            to_jsonb(COALESCE(CASE WHEN length(a.text) = 0 THEN NULL ELSE a.text END,'{"nm": "", "lvl_1": "", "lvl_1_nbr": "", "pg_nbr": ""}')::json) ->> 'nm' AS nm
         ,  to_jsonb(COALESCE(CASE WHEN length(a.text) = 0 THEN NULL ELSE a.text END,'{"nm": "", "lvl_1": "", "lvl_1_nbr": "", "pg_nbr": ""}')::json) ->> 'lvl_1' AS lvl_1
         ,  to_jsonb(COALESCE(CASE WHEN length(a.text) = 0 THEN NULL ELSE a.text END,'{"nm": "", "lvl_1": "", "lvl_1_nbr": "", "pg_nbr": ""}')::json) ->> 'lvl_2' AS lvl_2
         ,  to_jsonb(COALESCE(CASE WHEN length(a.text) = 0 THEN NULL ELSE a.text END,'{"nm": "", "lvl_1": "", "lvl_1_nbr": "", "pg_nbr": ""}')::json) ->> 'lvl_3' AS lvl_3
         , ARRAY_TO_STRING(a.tags,',') AS tags
         , (a.target_selectors ->> 1)::json ->> 'exact' AS txt
    FROM annotation AS a
    WHERE a.deleted = FALSE
)
SELECT
    nm
     , lvl_1
     , lvl_2
     , lvl_3
     , tags
     , txt
FROM idx_nt_tg
