
-   think about the outcome and what is fair; what would bad look like?
-   trade-off of using AI and explainability
-   interpretability
-   Ethics should reflect the ethics of the HCO
-   Cost function has to have the ethical principles baked into it - minimize the cost function
-   trade-offs with the cost function
-   bias vs variance trade-off
-   frameworks to generalize does not necessarily scale and work across problem domains
-   exploratory data analysis can check for bias - is there under reporting of someone?
-   causality vs correlation
    

[Fairness and Machine Learning](https://fairmlbook.org/index.html)
[Cloud Ethics](https://www.dukeupress.edu/Assets/PubMaterials/978-1-4780-0831-6_601.pdf)
[Toronto Data Workshop - Meggie Debnath, St. Michael’s Hospital & Unity Health Toronto](https://www.youtube.com/watch?v=ltA_vOwrIWc)
[10 20 2022 LHS Collaboratory "Explainability - AI and Ethics"](https://www.youtube.com/watch?v=Q8sB8m4dJ5c)
https://www.federalregister.gov/documents/2019/02/14/2019-02544/maintaining-american-leadership-in-artificial-intelligence
https://www.federalregister.gov/documents/2020/12/08/2020-27065/promoting-the-use-of-trustworthy-artificial-intelligence-in-the-federal-government

https://catalogofbias.org/

https://royalsocietypublishing.org/doi/full/10.1098/rsta.2018.0084
https://fairmlbook.org/
https://fairlearn.org/
https://wrongbutuseful.com/2020/04/20/towards-a-regulatory-framework-for-harmful-online-content-evaluating-reasonable-efforts/

https://healthyml.org/publication/

https://ec.europa.eu/futurium/en/ai-alliance-consultation/guidelines/1.html

https://www.strobe-statement.org/checklists/

